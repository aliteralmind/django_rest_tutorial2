command = '/home/jeffy/django_files/rest_tutorial/rest_venv/'
pythonpath = "/home/jeffy/django_files/rest_tutorial/rest_root/"
bind = '127.0.0.1:8001'
workers = 3
user = 'nobody'
error_logfile = 'logs/gunicorn_errors.log'
log_level = 'debug'

