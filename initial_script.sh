#!/bin/bash
#sudo find /home/jeffy/django_files/rest_tutorial/ -maxdepth 1 -mindepth 1 -not -name .git -not -name *.sublime-project -not -name initial_script.sh -exec rm -rf {} \;
baseDir="/home/jeffy/django_files/rest_tutorial/"
venvDir="${baseDir}rest_venv/"
djRootDir="${baseDir}rest_root/"

mkdir -p "$venvDir"
sudo virtualenv -p /usr/bin/python3.4 "$venvDir"

echo "source ${venvDir}bin/activate"
source "${venvDir}bin/activate"

echo -e "sudo \"${venvDir}bin/pip\" install django gunicorn psycopg2 ipython djangorestframework pygments httpie"
sudo "${venvDir}bin/pip" install django gunicorn psycopg2 ipython djangorestframework pygments httpie
#pygments for code highlighting
#httpie: Part 1 > Testing our first attempt at a Web API

sudo chown -R jeffy "$baseDir"
mkdir "$djRootDir"
django-admin.py startproject tutorial "$djRootDir"
cd "$djRootDir"
python manage.py startapp snippets

cd $baseDir
git init
git remote add origin https://aliteralmind@bitbucket.org/aliteralmind/django_rest_tutorial2.git
echo -e "*.class\n*.sublime-workspace" >> ${baseDir}.git/info/exclude

cp /home/jeffy/django_files/djauth_lifecycle_tutorial/final/git_add_commit_push_master*.sh .
chmod 774 git_add_commit_push_master*.sh

line1="command = '$venvDir'\n"
line2="pythonpath = \"$djRootDir\"\n"
line3="bind = '127.0.0.1:8001'\n"
line4="workers = 3\n"
line5="user = 'nobody'\n"
line6="error_logfile = 'logs/gunicorn_errors.log'\n"
line7="log_level = 'debug'\n"
all="$line1$line2$line3$line4$line5$line6$line7"
echo -e "$all" > "${venvDir}gunicorn_config.py"

mkdir "${venvDir}logs"  #So the gunicorn logs (above) have somewhere to write to.

#After models.py is created:
# python manage.py makemigrations snippets
# python manage.py migrate